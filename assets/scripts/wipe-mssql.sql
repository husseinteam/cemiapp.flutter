declare @n char(1)
set @n = char(10)

declare @stmt nvarchar(max)

-- views
select @stmt = isnull( @stmt + @n, '' ) + 
'drop view [' + sc.name + '].[' + vw.name + ']'
from sys.views vw
    inner join sys.schemas sc on vw.schema_id = sc.schema_id
WHERE vw.[type] = 'USER VIEW'

-- foreign keys
select @stmt = isnull( @stmt + @n, '' ) +
    'alter table [' + f.CONSTRAINT_SCHEMA + '].[' + f.TABLE_NAME + '] drop constraint [' + f.CONSTRAINT_NAME + ']'
from information_schema.table_constraints f
where f.CONSTRAINT_TYPE = 'FOREIGN KEY'

-- primary keys
select @stmt = isnull( @stmt + @n, '' ) +
    'alter table [' + f.CONSTRAINT_SCHEMA + '].[' + f.TABLE_NAME + '] drop constraint [' + f.CONSTRAINT_NAME + ']'
from information_schema.table_constraints f
where f.CONSTRAINT_TYPE = 'PRIMARY KEY'

-- unique keys
select @stmt = isnull( @stmt + @n, '' ) +
    'alter table [' + f.CONSTRAINT_SCHEMA + '].[' + f.TABLE_NAME + '] drop constraint [' + f.CONSTRAINT_NAME + ']'
from information_schema.table_constraints f
where f.CONSTRAINT_TYPE = 'UNIQUE'

-- tables
select @stmt = isnull( @stmt + @n, '' ) +
    'drop table [' + sc.name + '].[' + t.name + ']'
from sys.tables t
    inner join sys.schemas sc on t.schema_id = sc.schema_id AND
        t.TYPE = 'USER TABLE'

print @stmt
-- exec sp_executesql @stmt
