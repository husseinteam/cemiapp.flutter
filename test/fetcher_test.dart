import 'package:cemiapp/data/index.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Fetches DhikirGroups', () async {
    final dhikrGroups =
        await DBContext.fetcherOf<DhikrGroup>().fetchMany();
    expect(dhikrGroups, isNotNull);
    expect(dhikrGroups.length, greaterThan(0));
    final first = dhikrGroups.first;
    expect(first.dhikrItems, isNotNull);
    expect(first.dhikrItems.length, greaterThan(0));
    for (var dhikr in first.dhikrItems) {
      expect(dhikr.id, greaterThan(0));
    }
  });
}
