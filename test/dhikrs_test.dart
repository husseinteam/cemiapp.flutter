import 'package:cemiapp/api/index.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('api calls dhikrs with token', () async {
    final mapDhikrGroups =
        await Api.request.getList<Map<String, dynamic>>("dhikr-group");
    expect(mapDhikrGroups, isNotNull);
    for (var map in mapDhikrGroups) {
      for (var entry in map.entries) {
        expect(entry.key, isNotNull);
        expect(entry.value, isNotNull);
      }
    }

    final mapDhikrGroups2 =
        await Api.request.getList<Map<String, dynamic>>("dhikr-group");
    expect(mapDhikrGroups2, isNotNull);
    for (var map in mapDhikrGroups2) {
      for (var entry in map.entries) {
        expect(entry.key, isNotNull);
        expect(entry.value, isNotNull);
      }
    }
  });
}
