import 'package:async_redux/async_redux.dart';
import 'package:cemiapp/pages/app/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'pages/dhikr/dhikrGroup/screen.dart';
import 'pages/dhikr/dhikrGroup/states.dart';

Store<DhikrGroupState> store;
final navKey = GlobalKey<NavigatorState>();

void main() async {
  NavigateAction.setNavigatorKey(navKey);
  store = Store<DhikrGroupState>(initialState: DhikrGroupState.initial());
  runApp(Cemiapp());
}

class Cemiapp extends StatelessWidget {
  Cemiapp();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return StoreProvider<DhikrGroupState>(
      store: store,
      child: MaterialApp(
        title: 'Cemiapp',
        initialRoute: DhikrGroupScreen.routeName,
        navigatorKey: navKey,
        onGenerateRoute: Routes.reduce,
        theme: ThemeData(
          brightness: Brightness.dark,
        ),
      ),
    );
  }
}
