import 'package:async_redux/async_redux.dart';
import 'package:cemiapp/pages/dhikr/dhikrGroup/index.dart';
import 'package:flutter/material.dart';

class DhikrGroupScreen extends StatelessWidget {
  static const String routeName = "/";

  DhikrGroupScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<DhikrGroupState, DhikrGroupViewModel>(
      model: DhikrGroupViewModel(),
      onInit: (st) => st.dispatch(DispatchDhikrGroupsAction()),
      distinct: true,
      builder: (BuildContext context, DhikrGroupViewModel viewModel) =>
          DhikrGroupPage(vm: viewModel),
    );
  }
}
