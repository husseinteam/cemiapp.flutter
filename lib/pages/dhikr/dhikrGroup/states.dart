import 'package:cemiapp/entities/index.dart';
import 'package:meta/meta.dart';

@immutable
class DhikrGroupState {
  final User user;
  final int dhikrItemIndex;
  final int dhikrGroupIndex;
  final bool isLoading;
  final bool lastDhikrCompleted;
  final bool noDhikrCompleted;
  final bool editMode;

  DhikrGroupState({
    @required this.user,
    @required this.dhikrGroupIndex,
    @required this.dhikrItemIndex,
    @required this.isLoading,
    @required this.lastDhikrCompleted,
    @required this.noDhikrCompleted,
    @required this.editMode,
  });

  factory DhikrGroupState.initial() {
    return DhikrGroupState(
      user: User(),
      dhikrGroupIndex: 0,
      dhikrItemIndex: 0,
      isLoading: false,
      lastDhikrCompleted: false,
      noDhikrCompleted: true,
      editMode: false,
    );
  }

  DhikrItem get dhikrItem =>
      user.dhikrGroups[dhikrGroupIndex].dhikrItems[dhikrItemIndex];

  DhikrGroup get dhikrGroup => user.dhikrGroups[dhikrGroupIndex];

  DhikrGroupState copy({
    User user,
    int dhikrItemIndex,
    int dhikrGroupIndex,
    bool isLoading,
    bool lastDhikrCompleted,
    bool noDhikrCompleted,
    bool editMode,
  }) {
    return DhikrGroupState(
      user: user ?? this.user,
      dhikrGroupIndex: dhikrGroupIndex ?? this.dhikrGroupIndex,
      dhikrItemIndex: dhikrItemIndex ?? this.dhikrItemIndex,
      isLoading: isLoading ?? this.isLoading,
      lastDhikrCompleted: lastDhikrCompleted ?? this.lastDhikrCompleted,
      noDhikrCompleted: noDhikrCompleted ?? this.noDhikrCompleted,
      editMode: editMode ?? this.editMode,
    );
  }

  @override
  int get hashCode =>
      user.hashCode ^
      dhikrItemIndex.hashCode ^
      dhikrGroupIndex.hashCode ^
      isLoading.hashCode ^
      lastDhikrCompleted.hashCode ^
      noDhikrCompleted.hashCode ^
      editMode.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DhikrGroupState &&
          user == other.user &&
          dhikrItemIndex == other.dhikrItemIndex &&
          dhikrGroupIndex == other.dhikrGroupIndex &&
          isLoading == other.isLoading &&
          lastDhikrCompleted == other.lastDhikrCompleted &&
          noDhikrCompleted == other.noDhikrCompleted &&
          editMode == other.editMode;
}
