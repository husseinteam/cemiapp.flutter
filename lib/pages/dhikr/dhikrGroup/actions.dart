import 'package:async_redux/async_redux.dart';
import 'package:cemiapp/api/api.dart';
import 'package:cemiapp/data/index.dart';
import 'package:cemiapp/definitions/index.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:cemiapp/main.dart';
import 'package:cemiapp/pages/dhikr/dhikrGroup/index.dart';
import 'package:cemiapp/pages/dhikr/dhikrItem/screen.dart';

class DispatchDhikrGroupsAction extends ReduxAction<DhikrGroupState> {
  final bool fromApi;
  DispatchDhikrGroupsAction({this.fromApi: false});
  @override
  Future<DhikrGroupState> reduce() async {
    final userRepo = DBContext.repositoryOf<User>();
    int countUsers = await userRepo.count();
    if (fromApi || countUsers == 0) {
      final user = await Api.request.login().then((lresp) => lresp.user);
      user.dhikrGroups.sort((dgp, dgn) => dgp.inorder.compareTo(dgn.inorder));
      user.dhikrGroups.forEach((dg) =>
          dg.dhikrItems.sort((dip, din) => dip.inorder.compareTo(din.inorder)));
      await userRepo.deleteAll();
      return state.copy(user: await userRepo.insert(user));
    } else {
      return state.copy(user: (await userRepo.all()).first);
    }
  }

  void before() => dispatch(IsLoadingAction(true));

  void after() => dispatch(IsLoadingAction(false));
}

class IsLoadingAction extends ReduxAction<DhikrGroupState> {
  IsLoadingAction(this.val);

  final bool val;

  @override
  DhikrGroupState reduce() => state.copy(isLoading: val);
}

class ResetSubDhikrItemsAction extends ReduxAction<DhikrGroupState> {
  final User user;
  final int index;
  ResetSubDhikrItemsAction(this.user, this.index);

  @override
  Future<DhikrGroupState> reduce() async {
    final dhikrGroup = user.dhikrGroups[index];
    for (var dhikrItem in dhikrGroup.dhikrItems) {
      await dhikrItem.reset();
    }
    return state.copy(user: user);
  }
}

class ReorderDhikrGroupsAction extends ReduxAction<DhikrGroupState> {
  final User user;
  final int oldi;
  final int newi;
  ReorderDhikrGroupsAction(this.user, this.oldi, this.newi);

  @override
  Future<DhikrGroupState> reduce() async {
    final carrydg = user.dhikrGroups[oldi];
    carrydg.inorder = newi;
    final update = (DhikrGroup dg) async =>
        await DBContext.repositoryOf<DhikrGroup>()
            .updateColumns(dg, ['inorder']);
    //take out carry from list
    user.dhikrGroups.removeAt(oldi);

    user.dhikrGroups.asMap().forEach((i, dg) {
      //fix dhikrGroups after carrydg removal
      if (i >= oldi) {
        dg.inorder -= 1;
      }
      //fix dhikrGroups after carrydg insert
      if (i >= newi) {
        dg.inorder += 1;
      }
    });

    //insert carrydg to the newi position
    user.dhikrGroups.insert(newi, carrydg);

    //persist all
    for (var dg in user.dhikrGroups) {
      await update(dg);
    }
    return state.copy(user: user);
  }
}

class DeleteDhikrGroupAction extends ReduxAction<DhikrGroupState> {
  final User user;
  final int index;
  DeleteDhikrGroupAction(this.user, this.index);

  @override
  Future<DhikrGroupState> reduce() async {
    final repo = DBContext.repositoryOf<DhikrGroup>();
    final update =
        (DhikrGroup dg) async => await repo.updateColumns(dg, ['inorder']);
    await repo.delete(user.dhikrGroups[index]);
    user.dhikrGroups.removeAt(index);
    user.dhikrGroups.asMap().forEach((i, dg) {
      //fix dhikrGroups after carrydg removal
      if (i >= index) {
        dg.inorder -= 1;
      }
    });
    //persist all
    for (var dg in user.dhikrGroups) {
      await update(dg);
    }
    return state.copy(user: user);
  }
}

class DhikrGroupAddedAction extends ReduxAction<DhikrGroupState> {
  final User user;
  final DhikrGroup dhikrGroup;
  final FormType formType;
  DhikrGroupAddedAction(this.user, this.dhikrGroup, this.formType);

  @override
  Future<DhikrGroupState> reduce() async {
    final repo = DBContext.repositoryOf<DhikrGroup>();
    if (dhikrGroup.inorder > user.dhikrGroups.length) {
      dhikrGroup.inorder = user.dhikrGroups.length;
    } else if (dhikrGroup.inorder < 0) {
      dhikrGroup.inorder = 0;
    }
    final Function(DhikrGroup) upsert = (dg) async {
      if (formType == FormType.insert) {
        await repo.insert(dg);
      } else if (formType == FormType.edit) {
        await repo.update(dg);
      }
    };
    user.dhikrGroups.insert(dhikrGroup.inorder, dhikrGroup);
    user.dhikrGroups.asMap().forEach((i, dg) {
      //fix dhikrGroups after carrydg insert
      if (i >= dhikrGroup.inorder) {
        dg.inorder += 1;
      }
    });
    //persist all
    for (var dg in user.dhikrGroups) {
      await upsert(dg);
    }
    return state.copy(user: user);
  }
}

class DhikrGroupNavigatedAction extends ReduxAction<DhikrGroupState> {
  int dhikrGroupIndex;
  DhikrGroupNavigatedAction(this.dhikrGroupIndex);

  @override
  DhikrGroupState reduce() {
    navKey.currentState.pushNamed(DhikrItemScreen.routeName);
    return state.copy(dhikrGroupIndex: dhikrGroupIndex);
  }
}
