import 'package:async_redux/async_redux.dart';
import 'package:cemiapp/definitions/index.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:cemiapp/pages/app/index.dart';
import 'package:cemiapp/pages/dhikr/dhikrGroup/index.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class DhikrGroupViewModel extends BaseModel<DhikrGroupState> {
  User user;
  bool isLoading;
  bool editMode;
  List<int> dhikrGroupCounts;
  List<int> dhikrOrders;

  VoidCallback loadMore;
  VoidCallback getFromApi;
  Function(int) resetSubDhikrItems;
  Function(bool) setEditMode;
  Future<void> Function() onDispatch;
  Function(int, int) onReorder;
  Function(DhikrGroup, FormType) onDhikrGroupAdded;
  Function(int index) onDeleteDhikrGroup;

  Function(int) onNavigate;

  DhikrGroupViewModel();
  DhikrGroupViewModel.build({
    @required this.user,
    @required this.isLoading,
    @required this.editMode,
    @required this.dhikrGroupCounts,
    @required this.dhikrOrders,
    @required this.loadMore,
    @required this.getFromApi,
    @required this.resetSubDhikrItems,
    @required this.setEditMode,
    @required this.onDispatch,
    @required this.onReorder,
    @required this.onDhikrGroupAdded,
    @required this.onDeleteDhikrGroup,
    @required this.onNavigate,
  }) : super(equals: [isLoading]);

  @override
  DhikrGroupViewModel fromStore() => DhikrGroupViewModel.build(
        user: state.user,
        isLoading: state.isLoading,
        editMode: state.editMode,
        dhikrGroupCounts:
            state.user.dhikrGroups.map((dg) => dg.totalCount).toList(),
        dhikrOrders: state.user.dhikrGroups.map((dg) => dg.inorder).toList(),
        onDispatch: () => dispatchFuture(DispatchDhikrGroupsAction()),
        getFromApi: () =>
            dispatchFuture(DispatchDhikrGroupsAction(fromApi: true)),
        loadMore: () => dispatch(DispatchDhikrGroupsAction()),
        resetSubDhikrItems: (i) => dispatch(
          ResetSubDhikrItemsAction(state.user, i),
        ),
        setEditMode: (val) => dispatch(SetEditModeAction(val)),
        onReorder: (previ, nexti) => dispatch(
          ReorderDhikrGroupsAction(state.user, previ, nexti),
        ),
        onDhikrGroupAdded: (dg, ftype) => dispatch(
          DhikrGroupAddedAction(state.user, dg, ftype),
        ),
        onDeleteDhikrGroup: (i) => dispatch(
          DeleteDhikrGroupAction(state.user, i),
        ),
        onNavigate: (dhikrGroupIndex) {
          dispatch(DhikrGroupNavigatedAction(dhikrGroupIndex));
        },
      );

  @override
  int get hashCode =>
      user.hashCode ^
      isLoading.hashCode ^
      editMode.hashCode ^
      dhikrGroupCounts.hashCode ^
      dhikrOrders.hashCode ^
      user.dhikrGroups
          .map((dg) => dg.allDhikrsCompleted.hashCode)
          .reduce((acp, acn) => acp ^ acn);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DhikrGroupViewModel &&
          user == other.user &&
          isLoading == other.isLoading &&
          editMode == other.editMode &&
          dhikrGroupCounts == other.dhikrGroupCounts &&
          listEquals(dhikrOrders, other.dhikrOrders) &&
          listEquals(dhikrGroupCounts, other.dhikrGroupCounts);
}
