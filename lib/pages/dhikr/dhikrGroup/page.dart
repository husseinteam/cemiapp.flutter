import 'package:cemiapp/components/index.dart';
import 'package:cemiapp/definitions/index.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:cemiapp/pages/dhikr/dhikrGroup/index.dart';
import 'package:cemiapp/utils/index.dart';
import 'package:flutter/material.dart';

class DhikrGroupPage extends StatefulWidget {
  DhikrGroupPage({
    Key key,
    @required this.vm,
  }) : super(key: key);
  final DhikrGroupViewModel vm;
  @override
  _DhikrGroupPageState createState() => _DhikrGroupPageState();
}

class _DhikrGroupPageState extends State<DhikrGroupPage> {
  final headerbg =
      "https://img00.deviantart.net/35f0/i/2015/018/2/6/low_poly_landscape__the_river_cut_by_bv_designs-d8eib00.jpg";

  DhikrGroupViewModel get vm => widget.vm;
  ScrollController _controller;

  @override
  void initState() {
    _controller = ScrollController()
      ..addListener(() {
        if (!vm.isLoading &&
            _controller.position.maxScrollExtent ==
                _controller.position.pixels) {
          vm.loadMore();
        }
      });
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Kayıtlı Zikir Grupları"),
        actions: [
          vm.editMode
              ? FlatButton(
                  child: Text("Bitir", style: TextStyle(fontSize: 17.0)),
                  onPressed: () => vm.setEditMode(false),
                )
              : FlatButton(
                  child: Text("Düzenle", style: TextStyle(fontSize: 17.0)),
                  onPressed: () => vm.setEditMode(true),
                )
        ],
      ),
      body: SafeArea(
        child: SizedBox.expand(
          child: vm.isLoading
              ? Center(child: CircularProgressIndicator())
              : _buildListView(context),
        ),
      ),
      drawer: _buildDrawer(context),
      floatingActionButton: FloatingActionButton(
        elevation: 16.0,
        backgroundColor: Colors.green,
        tooltip: "Zikir Grubu Ekle",
        child: Icon(Icons.add, color: Colors.white),
        onPressed: () => _pushDhikrGroupForm(context),
      ),
    );
  }

  Drawer _buildDrawer(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            accountEmail: Text("hussein.sonmez@outlook.com"),
            accountName: Text("Hüseyin Sönmez"),
            currentAccountPicture: GestureDetector(
              child: CircleAvatar(
                child: Center(
                  child: Text("HS"),
                ),
              ),
              onTap: () => print("This is your current account."),
            ),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(headerbg),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Divider(),
          ListTile(
            title: Text("Veritabanını Sıfırla"),
            leading: Icon(Icons.refresh),
            onTap: () {
              Navigator.pop(context);
              vm.getFromApi();
            },
          ),
        ],
      ),
    );
  }

  Widget _buildListView(BuildContext context) {
    return vm.user.dhikrGroups.length == 0
        ? Center(child: Text("Henüz Zikir Grubu Eklenmemiş"))
        : ReorderableListView(
            onReorder: (oldIndex, newIndex) => vm.onReorder(
              oldIndex,
              oldIndex < newIndex ? newIndex - 1 : newIndex,
            ),
            scrollDirection: Axis.vertical,
            padding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 8.0),
            children: List<Widget>.generate(
              vm.user.dhikrGroups.length,
              (index) => Card(
                key: ObjectKey(index),
                clipBehavior: Clip.antiAlias,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: ListTile(
                  onTap: vm.editMode ? null : () => vm.onNavigate(index),
                  onLongPress: vm.editMode
                      ? null
                      : () => _onItemLongPress(context, index),
                  trailing: vm.editMode ? Icon(Icons.menu) : null,
                  leading: Stack(
                    alignment: Alignment.center,
                    children: [
                      Text(
                        "${(_evalCompletion(index) * 100).round()}%",
                        style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.w500,
                            fontSize: 13.0),
                      ),
                      SizedBox.fromSize(
                        size: Size.square(48.0),
                        child: CircularProgressIndicator(
                          value: _evalCompletion(index).toDouble(),
                          valueColor: AlwaysStoppedAnimation(
                            Colors.orange[400],
                          ),
                          strokeWidth: 1.4,
                        ),
                      ),
                    ],
                  ),
                  title: Text(
                    vm.user.dhikrGroups[index].name,
                    style: TextStyle(
                      color: Colors.green,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  subtitle: Text(
                    "Tamamlanan: ${vm.user.dhikrGroups[index].completedDhikrItemCount}/${vm.user.dhikrGroups[index].dhikrItems.length}",
                    style: TextStyle(
                      color: Colors.grey[300],
                      fontWeight: FontWeight.w100,
                    ),
                  ),
                ),
              ),
            ),
          );
  }

  _onItemLongPress(BuildContext context, int index) {
    ContextUtils.showActionSheet(
      context,
      title: "${vm.user.dhikrGroups[index]} Zikir Grubu:",
      message: "",
      actions: [
        SheetAction(
          title: "Sıfırla",
          subtitle: "${vm.user.dhikrGroups[index]} Sıfırlanmak Üzere.",
          onPress: () => vm.resetSubDhikrItems(index),
        ),
        SheetAction(
          title: "Sil",
          subtitle:
              "${vm.user.dhikrGroups[index]} ve bütün alt zikirler silinecek.",
          onPress: () => vm.onDeleteDhikrGroup(index),
          color: Colors.red,
        ),
      ],
    );
  }

  double _evalCompletion(int index) {
    final dg = vm.user.dhikrGroups[index];
    return dg.dhikrItems.length > 0
        ? dg.dhikrItems.map((d) => d.fixCount()).reduce((p, n) => p + n) /
            dg.dhikrItems.map((d) => d.target).reduce((p, n) => p + n)
        : 0.0;
  }

  void _pushDhikrGroupForm(context) async {
    final dg = await Navigator.push<DhikrGroup>(
      context,
      MaterialPageRoute(
        builder: (context) => EntityForm.of<DhikrGroup>(
          type: FormType.insert,
          owner: vm.user,
          specializer: (dg) => dg..inorder = vm.user.dhikrGroups.length,
        ),
      ),
    );
    vm.onDhikrGroupAdded(dg, FormType.insert);
  }
}
