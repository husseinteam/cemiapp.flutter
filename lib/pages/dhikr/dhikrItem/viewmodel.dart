import 'package:async_redux/async_redux.dart';
import 'package:cemiapp/definitions/index.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:cemiapp/pages/app/index.dart';
import 'package:cemiapp/pages/dhikr/dhikrGroup/states.dart';
import 'package:cemiapp/pages/dhikr/dhikrItem/index.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class DhikrItemViewModel extends BaseModel<DhikrGroupState> {
  DhikrGroup dhikrGroup;
  List<int> dhikrCounts;
  List<int> dhikrOrders;
  bool lastDhikrCompleted;
  bool noDhikrCompleted;
  bool editMode;

  Function(int, int) onReorder;
  Function(DhikrItem, FormType) onDhikrItemAdded;
  VoidCallback setLastDhikrCompletedToFalse;
  Function(bool) setNoDhikrCompletedTo;
  Function(bool) setEditMode;
  Function(int) resetDhikrItem;
  Function(int) deleteDhikrItem;

  Function(int) onNavigate;

  DhikrItemViewModel();
  DhikrItemViewModel.build({
    @required this.dhikrGroup,
    @required this.dhikrCounts,
    @required this.dhikrOrders,
    @required this.lastDhikrCompleted,
    @required this.noDhikrCompleted,
    @required this.editMode,
    @required this.onReorder,
    @required this.onDhikrItemAdded,
    @required this.setLastDhikrCompletedToFalse,
    @required this.setNoDhikrCompletedTo,
    @required this.setEditMode,
    @required this.resetDhikrItem,
    @required this.deleteDhikrItem,
    @required this.onNavigate,
  });

  @override
  DhikrItemViewModel fromStore() => DhikrItemViewModel.build(
        dhikrGroup: state.dhikrGroup,
        dhikrCounts: state.dhikrGroup.dhikrItems.map((di) => di.count).toList(),
        dhikrOrders:
            state.dhikrGroup.dhikrItems.map((di) => di.inorder).toList(),
        lastDhikrCompleted: state.lastDhikrCompleted,
        noDhikrCompleted: state.noDhikrCompleted,
        editMode: state.editMode,
        onReorder: (previ, nexti) => dispatch(
          ReorderDhikrItemsAction(
              state.user, state.dhikrGroupIndex, previ, nexti),
        ),
        onDhikrItemAdded: (di, ftype) => dispatch(
          AddDhikrItemAction(state.user, di, state.dhikrGroupIndex, ftype),
        ),
        setLastDhikrCompletedToFalse: () => dispatch(
          SetLastDhikrCompletedToFalseAction(),
        ),
        setNoDhikrCompletedTo: (val) => dispatch(
          SetNoDhikrCompletedToAction(val),
        ),
        setEditMode: (val) => dispatch(SetEditModeAction(val)),
        resetDhikrItem: (i) => dispatch(
            ResetDhikrItemAction(state.user, state.dhikrGroupIndex, i)),
        deleteDhikrItem: (i) => dispatch(
            DeleteDhikrItemAction(state.user, state.dhikrGroupIndex, i)),
        onNavigate: (dhikrItemIndex) => dispatch(
          DhikrItemNavigatedAction(dhikrItemIndex),
        ),
      );

  @override
  int get hashCode =>
      dhikrGroup.hashCode ^
      dhikrCounts.hashCode ^
      dhikrOrders.hashCode ^
      lastDhikrCompleted.hashCode ^
      noDhikrCompleted.hashCode ^
      editMode.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DhikrItemViewModel &&
          dhikrGroup == other.dhikrGroup &&
          lastDhikrCompleted == other.lastDhikrCompleted &&
          noDhikrCompleted == other.noDhikrCompleted &&
          editMode == other.editMode &&
          listEquals(dhikrCounts, other.dhikrCounts) &&
          listEquals(dhikrOrders, other.dhikrOrders);
}
