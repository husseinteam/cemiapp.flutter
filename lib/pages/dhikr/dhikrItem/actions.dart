import 'package:async_redux/async_redux.dart';
import 'package:cemiapp/data/index.dart';
import 'package:cemiapp/definitions/index.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:cemiapp/main.dart';
import 'package:cemiapp/pages/dhikr/dhikrCounter/index.dart';
import 'package:cemiapp/pages/dhikr/dhikrGroup/states.dart';

class ReorderDhikrItemsAction extends ReduxAction<DhikrGroupState> {
  final User user;
  final int dhikrGroupIndex;
  final int oldi;
  final int newi;
  ReorderDhikrItemsAction(
      this.user, this.dhikrGroupIndex, this.oldi, this.newi);

  @override
  Future<DhikrGroupState> reduce() async {
    final dhikrGroup = user.dhikrGroups[dhikrGroupIndex];
    final carrydi = dhikrGroup.dhikrItems[oldi];
    carrydi.inorder = newi;
    final update = (DhikrItem di) async =>
        await DBContext.repositoryOf<DhikrItem>()
            .updateColumns(di, ['inorder']);
    //take out carry from list
    dhikrGroup.dhikrItems.removeAt(oldi);

    dhikrGroup.dhikrItems.asMap().forEach((i, di) {
      //fix all dhikrItems after carrydi by substracting 1
      if (i >= oldi) {
        di.inorder -= 1;
      }

      //fix dhikrItems after carrydi
      if (i >= newi) {
        di.inorder += 1;
      }
    });

    //insert carrydi to the newi position
    dhikrGroup.dhikrItems.insert(newi, carrydi);

    //persist all
    for (var di in dhikrGroup.dhikrItems) {
      await update(di);
    }
    return state.copy(user: user);
  }
}

class AddDhikrItemAction extends ReduxAction<DhikrGroupState> {
  final User user;
  final DhikrItem dhikrItem;
  final int dhikrGroupIndex;
  final FormType formType;
  AddDhikrItemAction(
      this.user, this.dhikrItem, this.dhikrGroupIndex, this.formType);

  @override
  Future<DhikrGroupState> reduce() async {
    final repo = DBContext.repositoryOf<DhikrItem>();
    final dhikrGroup = user.dhikrGroups[dhikrGroupIndex];
    if (dhikrItem.inorder > dhikrGroup.dhikrItems.length ||
        dhikrItem.inorder < 0) {
      dhikrItem.inorder = dhikrGroup.dhikrItems.length;
    }
    if (formType == FormType.insert) {
      await repo.insert(dhikrItem);
    } else if (formType == FormType.edit) {
      await repo.update(dhikrItem);
    }
    dhikrGroup.dhikrItems.insert(dhikrItem.inorder, dhikrItem);
    return state.copy(user: user);
  }
}

class SetLastDhikrCompletedToFalseAction extends ReduxAction<DhikrGroupState> {
  SetLastDhikrCompletedToFalseAction();

  @override
  DhikrGroupState reduce() {
    return state.copy(
      lastDhikrCompleted: false,
    );
  }
}

class SetNoDhikrCompletedToAction extends ReduxAction<DhikrGroupState> {
  final bool noDhikrCompleted;
  SetNoDhikrCompletedToAction(this.noDhikrCompleted);

  @override
  DhikrGroupState reduce() {
    return state.copy(
      noDhikrCompleted: noDhikrCompleted,
    );
  }
}

class DhikrItemNavigatedAction extends ReduxAction<DhikrGroupState> {
  final int dhikrItemIndex;
  DhikrItemNavigatedAction(this.dhikrItemIndex);

  @override
  DhikrGroupState reduce() {
    navKey.currentState.pushNamed(DhikrCounterScreen.routeName);
    return state.copy(
      dhikrItemIndex: dhikrItemIndex,
    );
  }
}
