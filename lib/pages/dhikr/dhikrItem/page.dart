import 'package:cemiapp/components/index.dart';
import 'package:cemiapp/definitions/index.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:cemiapp/main.dart';
import 'package:cemiapp/pages/dhikr/dhikrItem/index.dart';
import 'package:cemiapp/utils/index.dart';
import 'package:flutter/material.dart';

class DhikrItemPage extends StatelessWidget {
  final DhikrItemViewModel vm;

  DhikrItemPage({
    @required this.vm,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _checkForBegin(context);
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("${vm.dhikrGroup}"),
        actions: [
          vm.editMode
              ? FlatButton(
                  child: Text("Bitir", style: TextStyle(fontSize: 17.0)),
                  onPressed: () => vm.setEditMode(false),
                )
              : FlatButton(
                  child: Text("Düzenle", style: TextStyle(fontSize: 17.0)),
                  onPressed: () => vm.setEditMode(true),
                )
        ],
      ),
      body: SafeArea(
        child: SizedBox.expand(
          child: vm.dhikrGroup.dhikrItems.length > 0
              ? _buildListView(context)
              : Center(
                  child: Text("Henüz Zikir Eklenmemiş"),
                ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        elevation: 16.0,
        backgroundColor: Colors.green,
        tooltip: "Zikir Ekle",
        child: Icon(Icons.add, color: Colors.white),
        onPressed: () => _pushDhikrItemForm(context),
      ),
    );
  }

  Future _checkForBegin(BuildContext context) async {
    if (vm.noDhikrCompleted) {
      Future.delayed(const Duration(milliseconds: 300), () async {
        await ContextUtils.showAlertDialog<String>(
          context,
          title: vm.dhikrGroup.appeal == null ? 'Zikir Listesi Hazır' : 'Niyet',
          message: vm.dhikrGroup.appeal == null
              ? "${vm.dhikrGroup} Zikir Listesine Sırayla Erişebileceksiniz"
              : vm.dhikrGroup.appeal,
          approvalText: "Başla",
          cancellationText: "Geri dön",
          onCancellation: () => navKey.currentState.pop(),
          onApproval: () => vm.setNoDhikrCompletedTo(false),
        );
      });
    }
    if (vm.lastDhikrCompleted) {
      Future.delayed(const Duration(milliseconds: 500), () async {
        await ContextUtils.showAlertDialog<String>(
          context,
          title: vm.dhikrGroup.endowment == null
              ? 'Zikir Grubu Tamamlandı'
              : 'Bağışlama',
          message: vm.dhikrGroup.endowment == null
              ? "${vm.dhikrGroup} Dahilindeki Bütün Zikirler Tamamlandı"
              : vm.dhikrGroup.endowment,
          approvalText: "Zikir Grubunu Bitir",
          cancellationText: "Sayfada Kal",
          onApproval: () => navKey.currentState.pop(),
        );
        vm.setLastDhikrCompletedToFalse();
        vm.setNoDhikrCompletedTo(true);
      });
    }
  }

  Widget _buildListView(BuildContext context) {
    final bool Function(int) active = (i) {
      final last = vm.dhikrGroup.dhikrItems
          .lastWhere((di) => di.completed, orElse: () => null);
      return i - 1 <= (last?.inorder ?? -1);
    };
    return ReorderableListView(
      onReorder: (oldIndex, newIndex) {
        if (active(oldIndex < newIndex ? newIndex - 1 : newIndex)) {
          vm.onReorder(
            oldIndex,
            oldIndex < newIndex ? newIndex - 1 : newIndex,
          );
        }
      },
      scrollDirection: Axis.vertical,
      padding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 8.0),
      children: List<Widget>.generate(
        vm.dhikrGroup.dhikrItems.length,
        (index) => AbsorbPointer(
          key: ObjectKey(index),
          absorbing: active(index) == false,
          child: Opacity(
            opacity: active(index) ? 1.0 : 0.3,
            child: Card(
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: ListTile(
                onTap: vm.editMode ? null : () => vm.onNavigate(index),
                trailing: vm.editMode ? Icon(Icons.menu) : null,
                onLongPress:
                    vm.editMode ? null : () => _onItemLongPress(context, index),
                leading: Stack(
                  alignment: Alignment.center,
                  children: [
                    Text(
                      "${(_evalCompletion(vm.dhikrGroup, index) * 100).round()}%",
                      style: TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.w500,
                          fontSize: 13.0),
                    ),
                    SizedBox.fromSize(
                      size: Size.square(48.0),
                      child: CircularProgressIndicator(
                        value: _evalCompletion(vm.dhikrGroup, index).toDouble(),
                        valueColor: AlwaysStoppedAnimation(
                          Colors.orange[400],
                        ),
                        strokeWidth: 1.4,
                      ),
                    ),
                  ],
                ),
                title: Text(
                  vm.dhikrGroup.dhikrItems[index].title,
                  style: TextStyle(
                    color: Colors.green,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                subtitle: Text(
                  "Tamamlanan: ${vm.dhikrGroup.dhikrItems[index].fixCount()}/${vm.dhikrGroup.dhikrItems[index].target}",
                  style: TextStyle(
                    color: Colors.grey[300],
                    fontWeight: FontWeight.w100,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  double _evalCompletion(DhikrGroup dhikrGroup, int index) {
    return vm.dhikrGroup.dhikrItems.length > 0
        ? vm.dhikrGroup.dhikrItems[index].fixCount() /
            vm.dhikrGroup.dhikrItems[index].target
        : 0.0;
  }

  _onItemLongPress(BuildContext context, int index) {
    ContextUtils.showActionSheet(
      context,
      title: "${vm.dhikrGroup.dhikrItems[index]} Zikri:",
      message: "",
      actions: [
        SheetAction(
          title: "Sıfırla",
          subtitle: "${vm.dhikrGroup.dhikrItems[index]} Sıfırlanmak Üzere.",
          onPress: () => vm.resetDhikrItem(index),
        ),
        SheetAction(
          title: "Sil",
          subtitle: "${vm.dhikrGroup.dhikrItems[index]} Zikri Silinecek.",
          onPress: () => vm.deleteDhikrItem(index),
          color: Colors.red,
        ),
      ],
    );
  }

  void _pushDhikrItemForm(context) async {
    var di = await Navigator.push<DhikrItem>(
      context,
      MaterialPageRoute(
        builder: (context) => EntityForm.of<DhikrItem>(
          type: FormType.insert,
          owner: vm.dhikrGroup,
          specializer: (di) => di..inorder = vm.dhikrGroup.dhikrItems.length,
        ),
      ),
    );
    vm.onDhikrItemAdded(di, FormType.insert);
  }
}
