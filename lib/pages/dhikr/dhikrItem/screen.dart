import 'package:async_redux/async_redux.dart';
import 'package:cemiapp/pages/dhikr/dhikrGroup/states.dart';
import 'package:cemiapp/pages/dhikr/dhikrItem/index.dart';
import 'package:flutter/material.dart';

class DhikrItemScreen extends StatelessWidget {
  static const String routeName = "/dhikrItem";

  DhikrItemScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<DhikrGroupState, DhikrItemViewModel>(
      model: DhikrItemViewModel(),
      distinct: true,
      builder: (BuildContext context, DhikrItemViewModel viewModel) =>
          DhikrItemPage(vm: viewModel),
    );
  }
}
