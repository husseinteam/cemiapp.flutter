import 'package:async_redux/async_redux.dart';
import 'package:cemiapp/pages/dhikr/dhikrCounter/index.dart';
import 'package:cemiapp/pages/dhikr/dhikrGroup/states.dart';
import 'package:flutter/material.dart';

class DhikrCounterScreen extends StatelessWidget {
  static const String routeName = "/dhikr-counter";

  DhikrCounterScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<DhikrGroupState, DhikrCounterViewModel>(
      model: DhikrCounterViewModel(),
      distinct: true,
      builder: (BuildContext context, DhikrCounterViewModel viewModel) =>
          DhikrCounterPage(vm: viewModel),
    );
  }
}
