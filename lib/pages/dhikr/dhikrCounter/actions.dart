import 'package:async_redux/async_redux.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:cemiapp/main.dart';
import 'package:cemiapp/pages/dhikr/dhikrGroup/states.dart';

class IncrementAction extends ReduxAction<DhikrGroupState> {
  DhikrItem dhikrItem;
  IncrementAction(this.dhikrItem);

  @override
  Future<DhikrGroupState> reduce() async {
    await dhikrItem.increment();
    return state.copy();
  }
}

class DhikrCounterNavigatedAction extends ReduxAction<DhikrGroupState> {
  final DhikrGroup dhikrGroup;
  final int dhikrItemIndex;
  DhikrCounterNavigatedAction(this.dhikrGroup, this.dhikrItemIndex);

  @override
  DhikrGroupState reduce() {
    navKey.currentState.pop();
    return state.copy(
      dhikrItemIndex: dhikrItemIndex,
      lastDhikrCompleted: dhikrGroup.allDhikrsCompleted &&
          dhikrGroup.dhikrItems[dhikrItemIndex].completed,
    );
  }
}
