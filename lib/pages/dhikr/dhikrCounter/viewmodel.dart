import 'package:async_redux/async_redux.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:cemiapp/pages/app/index.dart';
import 'package:cemiapp/pages/dhikr/dhikrCounter/index.dart';
import 'package:cemiapp/pages/dhikr/dhikrGroup/states.dart';
import 'package:flutter/material.dart';

class DhikrCounterViewModel extends BaseModel<DhikrGroupState> {
  DhikrItem dhikrItem;
  int count;
  Function() onIncrement;
  Function() onReset;
  Function() onDelete;
  Function() onCounterPop;

  DhikrCounterViewModel();

  DhikrCounterViewModel.build({
    @required this.dhikrItem,
    @required this.count,
    @required this.onIncrement,
    @required this.onReset,
    @required this.onDelete,
    @required this.onCounterPop,
  });

  @override
  DhikrCounterViewModel fromStore() => DhikrCounterViewModel.build(
        dhikrItem: state.dhikrItem,
        count: state.dhikrItem.count,
        onIncrement: () => dispatch(IncrementAction(state.dhikrItem)),
        onReset: () => dispatch(ResetDhikrItemAction(
            state.user, state.dhikrGroupIndex, state.dhikrItemIndex)),
        onDelete: () => dispatch(DeleteDhikrItemAction(
          state.user,
          state.dhikrGroupIndex,
          state.dhikrItemIndex,
        )),
        onCounterPop: () => dispatch(DhikrCounterNavigatedAction(
          state.dhikrGroup,
          state.dhikrItemIndex,
        )),
      );

  @override
  int get hashCode => dhikrItem.hashCode ^ count.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DhikrCounterViewModel && count == other.count;
}
