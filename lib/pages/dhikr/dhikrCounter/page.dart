import 'package:cemiapp/components/index.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:cemiapp/pages/dhikr/dhikrCounter/index.dart';
import 'package:cemiapp/utils/index.dart';
import 'package:flutter/material.dart';

class DhikrCounterPage extends StatelessWidget {
  final DhikrCounterViewModel vm;

  DhikrCounterPage({
    @required this.vm,
  });

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Tesbihat"),
          actions: [
            DhikrItemMenu(viewModel: vm),
          ],
        ),
        body: SafeArea(
          child: SizedBox.expand(
            child: _buildDhikrItem(context),
          ),
        ));
  }

  Padding _buildDhikrItem(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 8.0),
      child: InkWell(
        onTap: () => _onItemTap(context),
        onLongPress: () => _onItemLongPress(context),
        child: CounterCircle(vm.dhikrItem),
      ),
    );
  }

  void _onItemTap(BuildContext context) async {
    vm.onIncrement();
    if (vm.dhikrItem.completed) {
      VibrateUtils.feedbackFinal();
      ContextUtils.showActionSheet<String>(
        context,
        title: '${(vm.dhikrItem.owner as DhikrGroup).name}',
        message: '${vm.dhikrItem} Tamamlandı',
        actions: [
          SheetAction(
            title: 'Devam Et',
            color: Colors.green,
            onPress: () => vm.onCounterPop(),
          ),
          SheetAction(
            title: 'Burada Kal',
          ),
        ],
      );
    } else {
      await VibrateUtils.feedbackElement();
    }
  }

  void _onItemLongPress(BuildContext context) async {
    await ContextUtils.showActionSheet(
      context,
      title: 'İşleme Devam Edilsin mi?',
      message: "${vm.dhikrItem.title} Zikri Sıfırlanacak",
      actions: [
        SheetAction(
          title: 'Sıfırla',
          onPress: () => vm.onReset(),
        ),
      ],
    );
  }
}
