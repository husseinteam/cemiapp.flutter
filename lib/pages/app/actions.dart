import 'package:async_redux/async_redux.dart';
import 'package:cemiapp/data/index.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:cemiapp/pages/dhikr/dhikrGroup/index.dart';

class SetEditModeAction extends ReduxAction<DhikrGroupState> {
  final bool editMode;
  SetEditModeAction(this.editMode);
  @override
  DhikrGroupState reduce() {
    return state.copy(editMode: editMode);
  }
}

class ResetDhikrItemAction extends ReduxAction<DhikrGroupState> {
  final User user;
  final int dhikrGroupIndex;
  final int dhikrItemIndex;
  ResetDhikrItemAction(this.user, this.dhikrGroupIndex, this.dhikrItemIndex);

  @override
  Future<DhikrGroupState> reduce() async {
    final dhikrItem =
        user.dhikrGroups[dhikrGroupIndex].dhikrItems[dhikrItemIndex];
    await dhikrItem.reset();
    return state.copy(user: user);
  }
}

class DeleteDhikrItemAction extends ReduxAction<DhikrGroupState> {
  final User user;
  final int dhikrGroupIndex;
  final int dhikrItemIndex;
  DeleteDhikrItemAction(this.user, this.dhikrGroupIndex, this.dhikrItemIndex);

  @override
  Future<DhikrGroupState> reduce() async {
    final dhikrItemRepo = DBContext.repositoryOf<DhikrItem>();
    var dhikrItems = user.dhikrGroups[dhikrGroupIndex].dhikrItems;
    await dhikrItemRepo.delete(dhikrItems[dhikrItemIndex]);
    dhikrItems.removeAt(dhikrItemIndex);
    dhikrItems.asMap().forEach((i, di) {
      di.inorder = di.inorder - 1;
      dhikrItemRepo.updateColumns(di, ['inorder']);
    });
    return state.copy(user: user);
  }
}
