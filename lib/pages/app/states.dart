import 'package:cemiapp/pages/dhikr/dhikrGroup/states.dart';
import 'package:meta/meta.dart';

@immutable
class AppState {
  final DhikrGroupState dhikrGroupState;

  AppState({@required this.dhikrGroupState});

  factory AppState.initial() {
    return AppState(dhikrGroupState: DhikrGroupState.initial());
  }

  AppState copyWith({
    DhikrGroupState dhikrGroupState,
  }) {
    return AppState(
      dhikrGroupState: dhikrGroupState ?? this.dhikrGroupState,
    );
  }

  @override
  int get hashCode =>
      //isLoading.hash Code ^
      dhikrGroupState.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppState && dhikrGroupState == other.dhikrGroupState;
}
