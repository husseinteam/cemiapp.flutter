import 'package:cemiapp/pages/dhikr/dhikrGroup/index.dart';
import 'package:cemiapp/pages/dhikr/dhikrCounter/index.dart';
import 'package:cemiapp/pages/dhikr/dhikrItem/index.dart';
import 'package:flutter/material.dart';

class Routes {
  static Route<dynamic> reduce(RouteSettings settings) {
    final name = settings.name;
    // final args = List.from(settings.arguments ?? []);
    switch (name) {
      case DhikrGroupScreen.routeName:
        return MaterialPageRoute(builder: (context) => DhikrGroupScreen());
      case DhikrItemScreen.routeName:
        return MaterialPageRoute(builder: (context) => DhikrItemScreen());
      case DhikrCounterScreen.routeName:
        return MaterialPageRoute(builder: (context) => DhikrCounterScreen());
      default:
        return null;
    }
  }
}
