import 'package:connectivity/connectivity.dart';

class HttpUtils {
  static Future<ConnectivityResult> connectivity() async {
    ConnectivityResult result;
    try {
      result = await Connectivity().checkConnectivity();
    } catch (e) {
      throw ConnectivityException(e.toString());
    }
    return result;
  }
}

class ConnectivityException implements Exception {
  final message;
  ConnectivityException([this.message]);
}
