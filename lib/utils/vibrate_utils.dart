import 'package:vibrate/vibrate.dart';

class VibrateUtils {
  static Future feedbackFinal() async {
    if (await Vibrate.canVibrate) {
      await Vibrate.vibrate();
    }
  }

  static Future feedbackElement() async {
    if (await Vibrate.canVibrate) {
      Vibrate.feedback(FeedbackType.impact);
    }
  }

  static Future feedbackWarning() async {
    if (await Vibrate.canVibrate) {
      Vibrate.feedback(FeedbackType.warning);
    }
  }
}
