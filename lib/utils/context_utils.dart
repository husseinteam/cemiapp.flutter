import 'package:cemiapp/components/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ContextUtils {
  static void visitAllChildren(
      BuildContext context, Function(Element) visitor) {
    void visit(Element el) {
      visitor(el);
      el.visitChildren(visit);
    }

    (context as Element).visitChildren(visit);
  }

  static Future<T> showActionSheet<T>(
    BuildContext context, {
    @required String title,
    String message,
    Iterable<SheetAction> actions,
  }) async {
    final cancelButton = actions.firstWhere(
      (action) => action.isCancelButton,
      orElse: () => null,
    );
    return await showCupertinoModalPopup<T>(
      context: context,
      builder: (context) {
        return GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {},
          child: CupertinoActionSheet(
            title: Text(title),
            message: message == null || message.isEmpty ? null : Text(message),
            actions: actions.where((action) => !action.isCancelButton).map(
              (action) {
                List<Widget> actionWidgets = [
                  Text(
                    action.title,
                    style: TextStyle(fontSize: 24.0, color: action.color),
                  ),
                ];
                return CupertinoActionSheetAction(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: action.subtitle == null
                        ? actionWidgets
                        : (actionWidgets
                          ..addAll([
                            SizedBox(height: 8.0),
                            Text(
                              action.subtitle,
                              style: TextStyle(
                                  fontSize: 12.0, color: action.color),
                            ),
                          ])),
                  ),
                  onPressed: () {
                    if (action.response != null) {
                      Navigator.of(context, rootNavigator: true)
                          .pop(action.response);
                    } else {
                      Navigator.of(context, rootNavigator: true).pop();
                    }
                    if (action.onPress != null) {
                      action.onPress();
                    }
                  },
                  isDefaultAction: action.isDefault,
                );
              },
            ).toList(),
            cancelButton: cancelButton == null
                ? CupertinoActionSheetAction(
                    child: Text("İptal"),
                    isDefaultAction: true,
                    onPressed: () {
                      Navigator.of(context, rootNavigator: true).pop('cancel');
                    },
                  )
                : CupertinoActionSheetAction(
                    child: Text(cancelButton.title),
                    onPressed: () {
                      if (cancelButton.onPress != null) {
                        cancelButton.onPress();
                      }
                      Navigator.of(context, rootNavigator: true)
                          .pop(cancelButton.response);
                    },
                    isDefaultAction: cancelButton.isDefault,
                  ),
          ),
        );
      },
    );
  }

  static Future<T> showAlertDialog<T>(
    BuildContext context, {
    @required String title,
    @required String message,
    String approvalText,
    String cancellationText,
    void Function() onApproval,
    void Function() onCancellation,
  }) async {
    return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Text(
            title,
            style: TextStyle(color: Colors.green),
          ),
          content: Text(message),
          contentTextStyle: TextStyle(fontStyle: FontStyle.italic),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          elevation: 3.0,
          actions: [
            RaisedButton(
              color: Colors.blueAccent,
              onPressed: () {
                if (onApproval != null) {
                  onApproval();
                }
                Navigator.of(context, rootNavigator: true).pop();
              },
              child: Text(
                approvalText ?? "Tamam",
                style: TextStyle(fontSize: 12.0),
              ),
            ),
            RaisedButton(
              color: Colors.redAccent,
              onPressed: () {
                if (onCancellation != null) {
                  onCancellation();
                }
                Navigator.of(context, rootNavigator: true).pop();
              },
              child: Text(cancellationText ?? "Vazgeç",
                  style: TextStyle(fontSize: 12.0)),
            ),
          ],
        );
      },
    );
  }
}
