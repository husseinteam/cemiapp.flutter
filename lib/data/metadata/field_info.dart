import 'package:cemiapp/data/metadata/index.dart';
import 'package:cemiapp/definitions/index.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:flutter/material.dart';

class FieldInfo<TEntity extends GenericEntity<TEntity>> {
  final String name;
  final String repr;
  final Prop<TEntity, dynamic> prop;
  final DataType dataType;
  final bool nullable;
  final bool displayOnForm;
  final bool primaryKey;
  final bool uniqueKey;

  FieldInfo({
    @required this.name,
    @required this.repr,
    @required this.prop,
    this.dataType: DataType.text,
    this.nullable: false,
    this.displayOnForm: true,
    this.primaryKey: false,
    this.uniqueKey: false,
  });

  @override
  String toString() {
    final notNull = nullable ? "" : " NOT NULL";
    final unique = uniqueKey ? " UNIQUE" : "";
    final type = DataTypeRepr.of(dataType);
    return primaryKey
        ? "$name $type PRIMARY KEY NOT NULL"
        : "$name $type$unique$notNull";
  }
}
