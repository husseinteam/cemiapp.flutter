import 'package:cemiapp/data/metadata/index.dart';
import 'package:cemiapp/definitions/index.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:flutter/material.dart';

class TableInfo<TEntity extends GenericEntity<TEntity>> {
  final String repr;
  final String resource;
  final String tableName;
  final Iterable<FieldInfo<TEntity>> fieldInfos;
  final Iterable<ReferenceInfo> referenceInfos;
  final Iterable<CollectionInfo<TEntity>> collectionInfos;

  TableInfo({
    @required this.repr,
    @required this.resource,
    @required this.tableName,
    @required this.fieldInfos,
    this.referenceInfos,
    this.collectionInfos,
  }) : assert(fieldInfos.length > 0);

  @override
  String toString() {
    final fields = List.of([
      FieldInfo<TEntity>(
        repr: 'id',
        name: 'id',
        prop: Prop(
          getter: (e) => e.id,
          setter: (e, val) => e..id = val,
        ),
        dataType: DataType.int,
        primaryKey: true,
      )
    ])
      ..addAll(fieldInfos);
    var cols =
        fields.map((col) => "$col").reduce((col1, col2) => "$col1, $col2");
    final refCols = referenceInfos
            ?.map((ref) => ", ${ref.columnify()}")
            ?.reduce((col1, col2) => col1 + col2) ??
        '';
    final refs = referenceInfos
            ?.map((ref) => ', $ref')
            ?.reduce((ref1, ref2) => ref1 + ref2) ??
        '';
    return 'CREATE TABLE "$tableName"(${cols.trim()}${refCols.trim()}${refs.trim()});';
  }
}
