import 'package:cemiapp/data/metadata/index.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:flutter/material.dart';

class CollectionInfo<TEntity extends GenericEntity<TEntity>> {
  final String name;
  final CollectionProp<TEntity> collectionProp;
  CollectionInfo({
    @required this.name,
    @required this.collectionProp,
  });
}
