import 'package:cemiapp/definitions/index.dart';
import 'package:flutter/material.dart';

class ReferenceInfo {
  final String foreignTable;
  final String referencingColumn;
  final DataType dataType;
  final String foreignColumn;
  final int Function(dynamic) getForeignId;

  ReferenceInfo({
    @required this.foreignTable,
    @required this.referencingColumn,
    this.getForeignId,
    this.dataType: DataType.int,
    this.foreignColumn: "id",
  });

  @override
  String toString() {
    return 'FOREIGN KEY($referencingColumn) REFERENCES $foreignTable($foreignColumn)';
  }

  String columnify() {
    final type = DataTypeRepr.of(dataType);
    return "$referencingColumn $type NOT NULL";
  }
}
