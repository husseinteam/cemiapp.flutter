import 'dart:io';
import 'package:cemiapp/data/context.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  static Database _database;
  static final _version = 1;
  static final context = DBContext();

  Future close() async => _database.close();

  static Future<dynamic> resetDB({Database db}) async {
    final _db = db ?? _database;
    await _db.execute('PRAGMA foreign_keys = ON;');
    context.resetScripts().forEach((sql) async => await _db.execute(sql));
  }

  static Future<TResult> commitTransaction<TResult>(
      Future<TResult> Function(Transaction) commit) async {
    return await getDB().then((db) async => await db.transaction(commit));
  }

  static Future<Database> getDB() async {
    Directory databaseDirectory = await getApplicationDocumentsDirectory();
    final dbFile = File(join(databaseDirectory.path, "hkk.db"));
    if (_database == null) {
      _database = await openDatabase(
        dbFile.path,
        version: _version,
        onUpgrade: (db, prev, next) async {
          await resetDB(db: db);
        },
        onOpen: (db) async {
          //await resetDB(db: db);
        },
        onCreate: (Database db, int version) async {
          await resetDB(db: db);
        },
      );
    }
    return _database;
  }
}
