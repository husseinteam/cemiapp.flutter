import 'package:cemiapp/api/index.dart';
import 'package:cemiapp/data/index.dart';
import 'package:cemiapp/entities/index.dart';

class Fetcher<T extends GenericEntity<T>> {
  final T Function() _tGenerator;
  Fetcher(this._tGenerator);

  Future<List<T>> fetchMany() async {
    final repo = DBContext.repositoryOf<T>();
    final itemCount = await repo.count();
    if (itemCount > 0) {
      return await repo.all();
    }
    final response =
        await Api.request.single("${_tGenerator().tableInfo.resource}/");
    final listOfT = List<T>();
    for (var map in response['all']) {
      final item = _tGenerator().fromMap(map, omitId: true);
      await repo.insert(item);
      listOfT.add(item);
    }
    return listOfT;
  }

  Future<T> fetchOne(int id) async {
    final one =
        await Api.request.single("${_tGenerator().tableInfo.resource}/$id");
    return _tGenerator().fromMap(one, omitId: true);
  }
}
