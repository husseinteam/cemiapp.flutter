import 'package:cemiapp/data/index.dart';
import 'package:cemiapp/definitions/index.dart';
import 'package:cemiapp/entities/index.dart';

class DBContext {
  static Iterable<ContextViewGeneratorDelegate> _viewGenerators = [
    () => User(),
    () => DhikrGroup(),
    () => DhikrItem(),
  ];

  static T instanceOf<T extends GenericEntity<T>>() {
    return _viewGenerators.where((vgen) => vgen().runtimeType == T).first()
        as T;
  }

  static Repository<T> repositoryOf<T extends GenericEntity<T>>() {
    return Repository(() => DBContext.instanceOf<T>());
  }

  static Fetcher<T> fetcherOf<T extends GenericEntity<T>>() {
    return Fetcher(() => DBContext.instanceOf<T>());
  }

  Iterable<String> resetScripts() {
    var scripts = List<String>()
      ..addAll(_dropScripts)
      ..addAll(_generationScripts);
    return scripts;
  }

  Iterable<String> get _generationScripts {
    var scriptList = List<String>();
    for (var vgen in _viewGenerators) {
      final table = vgen().tableInfo;
      scriptList.add('$table');
    }
    return scriptList;
  }

  Iterable<String> get _dropScripts {
    var scriptList = List<String>();
    for (var vgen in _viewGenerators) {
      final table = vgen().tableInfo;
      scriptList.add('DROP TABLE IF EXISTS "${table.tableName}"');
    }
    return scriptList;
  }
}
