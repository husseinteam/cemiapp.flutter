import 'package:cemiapp/data/index.dart';
import 'package:cemiapp/data/metadata/index.dart';
import 'package:cemiapp/entities/index.dart';

class Repository<T extends GenericEntity<T>> {
  final T Function() generatorEntity;
  Repository(this.generatorEntity);

  Future<List<T>> all({
    String whereString,
    List<dynamic> args,
  }) async {
    var t = generatorEntity();
    var map = await DBProvider.commitTransaction(
      (txn) async => await txn.query(
        t.tableInfo.tableName,
        where: whereString,
        whereArgs: args,
      ),
    );
    List<T> list = [];
    if (map.isNotEmpty) {
      for (var itemMap in map) {
        t = t.fromMap(itemMap);
        for (var collInfo in t.tableInfo.collectionInfos ?? []) {
          List<dynamic> items;
          final TableInfo itemTableInfo =
              collInfo.collectionProp.itemRepo.generatorEntity().tableInfo;
          final ReferenceInfo itemReferenceInfo =
              itemTableInfo.referenceInfos.firstWhere(
            (refInfo) => refInfo.foreignTable == t.tableInfo.tableName,
            orElse: null,
          );
          final whereString = "${itemReferenceInfo.referencingColumn}=?";
          items = await collInfo.collectionProp.itemRepo.all(
            whereString: whereString,
            args: [t.id],
          );
          items.forEach((it) => it.owner = t);
          collInfo.collectionProp.setter(t, items);
        }
        list.add(t);
      }
    }
    return list;
  }

  Future<int> deleteAll({TableInfo ti}) async {
    int total = 0;
    final _ti = ti ?? (generatorEntity().tableInfo);
    final childCount = (_ti.collectionInfos ?? []).length;
    if (childCount > 0) {
      for (var ci in _ti.collectionInfos) {
        final childTi = ci.collectionProp.itemRepo.generatorEntity().tableInfo;
        total += await deleteAll(ti: childTi);
      }
    }
    total += await DBProvider.commitTransaction(
      (txn) async {
        int c = await txn.rawDelete("DELETE from ${_ti.tableName}");
        return c;
      },
    );
    return total;
  }

  Future<T> single(int id) async {
    var map = await DBProvider.commitTransaction(
      (txn) async => await txn.query(
        generatorEntity().tableInfo.tableName,
        where: 'id = ?',
        whereArgs: [id],
      ),
    );
    return map.isNotEmpty ? (generatorEntity().fromMap(map.first)) : null;
  }

  Future<int> count() async {
    final map = await DBProvider.commitTransaction((txn) async =>
        await txn.rawQuery(
            "SELECT COUNT(*) AS C FROM ${generatorEntity().tableInfo.tableName}"));
    return map.first["C"].toInt();
  }

  Future<T> insert(T entity) async {
    entity.id = await DBProvider.commitTransaction(
      (txn) async => await txn.insert(
        entity.tableInfo.tableName,
        entity.toMap(),
      ),
    );

    for (CollectionInfo<T> collInfo in entity.tableInfo.collectionInfos ?? []) {
      var items = collInfo.collectionProp.getter(entity);
      for (var item in items) {
        item.owner = entity;
        await collInfo.collectionProp.itemRepo.insert(item);
      }
    }
    return entity;
  }

  Future<int> update(T entity) async {
    return await DBProvider.commitTransaction(
      (txn) async => await txn.update(
        entity.tableInfo.tableName,
        entity.toMap(),
        where: "id = ?",
        whereArgs: [entity.id],
      ),
    );
  }

  Future<int> updateColumns(T entity, List<String> columns) async {
    return await DBProvider.commitTransaction(
      (txn) async => await txn.update(
        entity.tableInfo.tableName,
        Map.fromEntries(columns.map((colName) => MapEntry(
            colName, entity.fieldInfo(withName: colName).prop.getter(entity)))),
        where: "id = ?",
        whereArgs: [entity.id],
      ),
    );
  }

  Future<int> delete(T entity) async {
    var deletedCount = await DBProvider.commitTransaction(
      (txn) async => await txn.delete(
        entity.tableInfo.tableName,
        where: "id = ?",
        whereArgs: [entity.id],
      ),
    );

    for (var collInfo in entity.tableInfo.collectionInfos ?? []) {
      var items = collInfo.collectionProp.getter(entity);
      for (var item in items) {
        deletedCount += await collInfo.collectionProp.itemRepo.delete(item);
      }
    }

    return deletedCount;
  }

  Future<T> upsert(T entity) async {
    final existing = await DBContext.repositoryOf<T>().single(entity.id);
    if (existing == null) {
      return await this.insert(entity);
    } else {
      return existing;
    }
  }
}
