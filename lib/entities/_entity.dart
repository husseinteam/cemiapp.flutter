import 'package:cemiapp/data/index.dart';
import 'package:cemiapp/data/metadata/index.dart';
import 'package:flutter/material.dart';

abstract class GenericEntity<TEntity extends GenericEntity<TEntity>> {
  int id = 0;
  TableInfo<TEntity> get tableInfo;
  bool search(String query);
  GenericEntity owner;

  @override
  String toString();

  FieldInfo<TEntity> fieldInfo({@required String withName}) =>
      tableInfo.fieldInfos.firstWhere((fi) => fi.name == withName);

  TEntity fromMap(Map<String, dynamic> map,
      {GenericEntity withOwner, bool omitId = false}) {
    var t = DBContext.instanceOf<TEntity>();
    t.owner = withOwner ?? owner;
    if (omitId == false) {
      t.id = map["id"];
    }
    tableInfo.fieldInfos.forEach((fi) {
      if (fi.prop.defaultValueGetter != null) {
        fi.prop.setter(t, fi.prop.defaultValueGetter(t));
      } else {
        fi.prop.setter(t, map[fi.name]);
      }
    });
    if (tableInfo.collectionInfos != null) {
      tableInfo.collectionInfos.forEach(
        (ci) => ci.collectionProp.setter(
            t,
            (map[ci.name] ?? [])
                .map((itemMap) => ci.collectionProp.itemRepo
                    .generatorEntity()
                    .fromMap(itemMap, withOwner: this, omitId: omitId))
                .toList()),
      );
    }

    return t;
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    for (var field in tableInfo.fieldInfos) {
      map[field.name] = field.prop.getter(this as TEntity);
    }
    for (var field in tableInfo.referenceInfos ?? []) {
      final getter = field.getForeignId ?? (e) => e.id;
      map[field.referencingColumn] = getter(this.owner);
    }
    return map;
  }
}
