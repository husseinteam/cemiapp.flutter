import 'package:cemiapp/data/index.dart';
import 'package:cemiapp/data/metadata/index.dart';
import 'package:cemiapp/definitions/index.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:flutter/foundation.dart';

class DhikrGroup extends GenericEntity<DhikrGroup> {
  String name;
  String appeal;
  String endowment;
  int inorder;

  List<DhikrItem> dhikrItems = [];
  DhikrGroup({
    this.name,
    this.appeal,
    this.endowment,
    this.inorder: 0,
  });

  int get totalCount => dhikrItems.length == 0
      ? 0
      : dhikrItems.map((di) => di.count).reduce((dcp, dcn) => dcp + dcn);

  int get completedDhikrItemCount => dhikrItems.length == 0
      ? 0
      : dhikrItems.where((di) => di.completed).toList().length;

  bool get allDhikrsCompleted => completedDhikrItemCount == dhikrItems.length;

  void swapDhikrItems(int oldi, int newi) {
    var di = dhikrItems[oldi];
    dhikrItems[oldi] = dhikrItems[newi];
    dhikrItems[newi] = di;
  }

  @override
  bool search(String query) => this.toString().startsWith(query);

  @override
  String toString() => name;

  @override
  TableInfo<DhikrGroup> get tableInfo => TableInfo<DhikrGroup>(
        tableName: "dhikr_groups",
        resource: "dhikr-group",
        repr: "Zikir Grubu",
        fieldInfos: [
          FieldInfo(
            name: "name",
            repr: "İsim",
            prop: Prop(
              getter: (e) => e.name,
              setter: (e, val) => e..name = val,
            ),
          ),
          FieldInfo(
            name: "appeal",
            repr: "Niyet",
            prop: Prop(
              getter: (e) => e.appeal,
              setter: (e, val) => e..appeal = val,
            ),
            nullable: true,
          ),
          FieldInfo(
            name: "endowment",
            repr: "Bağışlama",
            prop: Prop(
              getter: (e) => e.endowment,
              setter: (e, val) => e..endowment = val,
            ),
            nullable: true,
          ),
          FieldInfo(
            name: "inorder",
            repr: "Sıralama",
            dataType: DataType.int,
            prop: Prop(
              getter: (e) => e.inorder,
              setter: (e, val) => e..inorder = val,
            ),
          ),
        ],
        collectionInfos: [
          CollectionInfo(
            name: "dhikrItems",
            collectionProp: CollectionProp(
              getter: (e) => e.dhikrItems,
              setter: (v, items) =>
                  v..dhikrItems = items.cast<DhikrItem>().toList(),
              itemRepo: DBContext.repositoryOf<DhikrItem>(),
            ),
          )
        ],
        referenceInfos: [
          ReferenceInfo(
            foreignTable: DBContext.instanceOf<User>().tableInfo.tableName,
            referencingColumn: "user_id",
          ),
        ],
      );

  @override
  int get hashCode =>
      name.hashCode ^ appeal.hashCode ^ endowment.hashCode ^ inorder;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DhikrGroup &&
          name == other.name &&
          appeal == other.appeal &&
          endowment == other.endowment &&
          inorder == other.inorder &&
          listEquals(dhikrItems, other.dhikrItems);
}
