import 'package:cemiapp/data/context.dart';
import 'package:cemiapp/data/metadata/index.dart';
import 'package:cemiapp/definitions/index.dart';
import 'package:cemiapp/entities/index.dart';

class DhikrItem extends GenericEntity<DhikrItem> {
  String title;
  String transcript;
  String arabic;
  int count;
  int target;
  int inorder;
  DhikrItem({
    this.title,
    this.transcript,
    this.arabic,
    this.target,
    this.count: 0,
    this.inorder: 0,
  });

  bool get completed => fixCount() == target;

  int fixCount() => count = count >= target ? target : count;

  Future increment() async {
    count = count >= target ? target : count + 1;
    await DBContext.repositoryOf<DhikrItem>().updateColumns(this, ['count']);
  }

  Future reset() async {
    count = 0;
    await DBContext.repositoryOf<DhikrItem>().updateColumns(this, ['count']);
  }

  @override
  bool search(String query) =>
      this.title.startsWith(query) || this.transcript.startsWith(query);

  @override
  String toString() => title;

  @override
  TableInfo<DhikrItem> get tableInfo => TableInfo<DhikrItem>(
        tableName: "dhikr_items",
        resource: "dhikr-item",
        repr: "Zikir",
        fieldInfos: [
          FieldInfo(
            name: "title",
            repr: "İsim",
            prop: Prop(
              getter: (e) => e.title,
              setter: (e, val) => e..title = val,
            ),
          ),
          FieldInfo(
            name: "transcript",
            repr: "Zikir",
            prop: Prop(
              getter: (e) => e.transcript,
              setter: (e, val) => e..transcript = val,
            ),
          ),
          FieldInfo(
            name: "arabic",
            repr: "Arapça",
            nullable: true,
            displayOnForm: false,
            prop: Prop(
              getter: (e) => e.arabic,
              setter: (e, val) => e..arabic = val,
            ),
          ),
          FieldInfo(
            name: "target",
            repr: "Hedef",
            dataType: DataType.int,
            prop: Prop(
              getter: (e) => e.target,
              setter: (e, val) => e..target = val,
            ),
          ),
          FieldInfo(
            name: "count",
            repr: "Mevcut",
            dataType: DataType.int,
            nullable: true,
            displayOnForm: false,
            prop: Prop(
              getter: (e) => e.count,
              setter: (e, val) => e..count = val,
              defaultValueGetter: (e) => 0
            ),
          ),
          FieldInfo(
            name: "inorder",
            repr: "Sıralama",
            dataType: DataType.int,
            prop: Prop(
              getter: (e) => e.inorder,
              setter: (e, val) => e..inorder = val,
            ),
          ),
        ],
        referenceInfos: [
          ReferenceInfo(
            foreignTable:
                DBContext.instanceOf<DhikrGroup>().tableInfo.tableName,
            referencingColumn: "dhikr_group_id",
          ),
        ],
      );

  @override
  int get hashCode =>
      title.hashCode ^
      transcript.hashCode ^
      arabic.hashCode ^
      target ^
      count ^
      inorder;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DhikrItem &&
          title == other.title &&
          transcript == other.transcript &&
          arabic == other.arabic &&
          target == other.target &&
          count == other.count &&
          inorder == other.inorder;
}
