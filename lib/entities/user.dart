import 'package:cemiapp/data/index.dart';
import 'package:cemiapp/data/metadata/index.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:flutter/foundation.dart';

class User extends GenericEntity<User> {
  String email;
  String identity;
  String apiToken;
  String password;

  List<DhikrGroup> dhikrGroups = [];

  User({
    this.email,
    this.identity,
    this.apiToken,
    this.password,
  });

  void swapDhikrGroups(int oldi, int newi) {
    var dg = dhikrGroups[oldi];
    dhikrGroups[oldi] = dhikrGroups[newi];
    dhikrGroups[newi] = dg;
  }

  @override
  bool search(String query) =>
      this.email.startsWith(query) || this.identity.startsWith(query);

  @override
  String toString() => identity;

  @override
  TableInfo<User> get tableInfo => TableInfo<User>(
        tableName: "users",
        resource: "user",
        repr: "Kullanıcı",
        fieldInfos: [
          FieldInfo(
            name: "email",
            repr: "E-Posta",
            prop: Prop(
              getter: (e) => e.email,
              setter: (e, val) => e..email = val,
            ),
            nullable: true,
          ),
          FieldInfo(
            name: "identity",
            repr: "Kimlik",
            prop: Prop(
              getter: (e) => e.identity,
              setter: (e, val) => e..identity = val,
            ),
          ),
          FieldInfo(
            name: "apiToken",
            repr: "Giriş Anahtarı",
            prop: Prop(
              getter: (e) => e.apiToken,
              setter: (e, val) => e..apiToken = val,
            ),
            nullable: true,
          ),
          FieldInfo(
            name: "password",
            repr: "Şifre",
            prop: Prop(
              getter: (e) => e.password,
              setter: (e, val) => e..password = val,
            ),
          ),
        ],
        collectionInfos: [
          CollectionInfo(
            name: "dhikrGroups",
            collectionProp: CollectionProp(
              getter: (e) => e.dhikrGroups,
              setter: (v, items) =>
                  v..dhikrGroups = items.cast<DhikrGroup>().toList(),
              itemRepo: DBContext.repositoryOf<DhikrGroup>(),
            ),
          )
        ],
      );

  @override
  int get hashCode =>
      email.hashCode ^
      identity.hashCode ^
      apiToken.hashCode ^
      password.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is User &&
          email == other.email &&
          identity == other.identity &&
          apiToken == other.apiToken &&
          password == other.password &&
          listEquals(dhikrGroups, other.dhikrGroups);
}
