import 'package:cemiapp/data/index.dart';
import 'package:cemiapp/data/metadata/field_info.dart';
import 'package:cemiapp/definitions/index.dart';
import 'package:cemiapp/entities/index.dart';
import 'package:cemiapp/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class EntityForm {
  static of<TEntity extends GenericEntity<TEntity>>({
    @required FormType type,
    @required GenericEntity owner,
    TEntity entity,
    TEntity Function(TEntity) specializer,
  }) =>
      _FormPage<TEntity>(
          type,
          owner,
          type == FormType.edit ? entity : DBContext.instanceOf<TEntity>(),
          specializer);
}

class _FormPage<TEntity extends GenericEntity<TEntity>>
    extends StatelessWidget {
  _FormPage(this.type, this.owner, this.entity, this.specializer, {Key key})
      : super(key: key);
  final FormType type;
  final GenericEntity owner;
  final TEntity entity;
  final TEntity Function(TEntity) specializer;
  final GlobalKey<FormBuilderState> _fbKey = new GlobalKey<FormBuilderState>();
  final Map values = Map<String, dynamic>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          type == FormType.edit
              ? "Düzenle: $entity"
              : "Yeni Ekle: ${entity.tableInfo.repr}",
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () {
              if (_fbKey.currentState.saveAndValidate()) {
                navKey.currentState.pop(
                  entity.fromMap(
                    _fbKey.currentState.value,
                    withOwner: owner,
                  ),
                );
              }
            },
          )
        ],
      ),
      body: SafeArea(
        child: SizedBox.expand(child: _buildForm(context)),
      ),
    );
  }

  Widget _buildForm(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 30.0),
        child: Column(
          children: <Widget>[
            FormBuilder(
              key: _fbKey,
              initialValue: {
                'date': DateTime.now(),
                'accept_terms': false,
              },
              autovalidate: true,
              child: Column(
                children: entity.tableInfo.fieldInfos
                    .where((fi) => fi.displayOnForm)
                    .map((field) => _buildTextField(context, field))
                    .toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTextField(
    BuildContext context,
    FieldInfo<TEntity> field,
  ) {
    // FormBuilderDateTimePicker(
    //   attribute: "date",
    //   inputType: InputType.date,
    //   format: DateFormat("yyyy-MM-dd"),
    //   decoration:
    //       InputDecoration(labelText: "Appointment Time"),
    // ),
    // FormBuilderSlider(
    //   attribute: "slider",
    //   validators: [FormBuilderValidators.min(6)],
    //   min: 0.0,
    //   max: 10.0,
    //   initialValue: 1.0,
    //   divisions: 20,
    //   decoration:
    //       InputDecoration(labelText: "Number of things"),
    // ),
    // FormBuilderCheckbox(
    //   attribute: 'accept_terms',
    //   label: Text(
    //       "I have read and agree to the terms and conditions"),
    //   validators: [
    //     FormBuilderValidators.requiredTrue(
    //       errorText:
    //           "You must accept terms and conditions to continue",
    //     ),
    //   ],
    // ),
    // FormBuilderDropdown(
    //   attribute: "gender",
    //   decoration: InputDecoration(labelText: "Gender"),
    //   // initialValue: 'Male',
    //   hint: Text('Select Gender'),
    //   validators: [FormBuilderValidators.required()],
    //   items: ['Male', 'Female', 'Other']
    //     .map((gender) => DropdownMenuItem(
    //        value: gender,
    //        child: Text("$gender")
    //   )).toList(),
    // ),
    final isInt = field.dataType == DataType.int;
    var validators = List<String Function(dynamic)>();
    dynamic Function(dynamic) transformer = (v) => v;
    if (isInt) {
      validators.add(
        FormBuilderValidators.numeric(errorText: "Metin Rakamlardan Oluşmalı"),
      );
      transformer = (v) => int.parse(v.toString());
    }
    if (field.nullable == false) {
      validators.add(
        FormBuilderValidators.required(
          errorText: "${field.repr} Alanını Boş Bırakmayınız.",
        ),
      );
    }
    return FormBuilderTextField(
      attribute: field.name,
      initialValue: type == FormType.edit
          ? (isInt
              ? int.parse(field.prop.getter(entity)).toString()
              : field.prop.getter(entity).toString())
          : null,
      decoration: InputDecoration(
        labelText: field.repr,
        helperText: isInt ? "Sayı Giriniz" : "Metin Giriniz",
      ),
      validators: validators,
      valueTransformer: transformer,
      keyboardAppearance: Brightness.dark,
      keyboardType: isInt
          ? TextInputType.numberWithOptions(decimal: false, signed: false)
          : TextInputType.text,
    );
    // FormBuilderSegmentedControl(
    //   decoration:
    //       InputDecoration(labelText: "Movie Rating (Archer)"),
    //   attribute: "movie_rating",
    //   options: List.generate(5, (i) => i + 1)
    //       .map(
    //           (number) => FormBuilderFieldOption(value: number))
    //       .toList(),
    // ),
    // FormBuilderSwitch(
    //   label: Text('I Accept the tems and conditions'),
    //   attribute: "accept_terms_switch",
    //   initialValue: true,
    // ),
    // FormBuilderStepper(
    //   decoration: InputDecoration(labelText: "Stepper"),
    //   attribute: "stepper",
    //   initialValue: 10,
    //   step: 1,
    // ),
    // FormBuilderRate(
    //   decoration: InputDecoration(labelText: "Rate this form"),
    //   attribute: "rate",
    //   iconSize: 32.0,
    //   initialValue: 1,
    //   max: 5,
    // ),
    // FormBuilderCheckboxList(
    //   decoration:
    //   InputDecoration(labelText: "The language of my people"),
    //   attribute: "languages",
    //   initialValue: ["Dart"],
    //   options: [
    //     FormBuilderFieldOption(value: "Dart"),
    //     FormBuilderFieldOption(value: "Kotlin"),
    //     FormBuilderFieldOption(value: "Java"),
    //     FormBuilderFieldOption(value: "Swift"),
    //     FormBuilderFieldOption(value: "Objective-C"),
    //   ],
    // ),
    // FormBuilderSignaturePad(
    //   decoration: InputDecoration(labelText: "Signature"),
    //   attribute: "signature",
    //   height: 100,
    // ),
  }
}
