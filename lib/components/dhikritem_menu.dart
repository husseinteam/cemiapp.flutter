import 'package:cemiapp/components/index.dart';
import 'package:cemiapp/main.dart';
import 'package:cemiapp/pages/dhikr/dhikrCounter/index.dart';
import 'package:cemiapp/utils/index.dart';
import 'package:flutter/material.dart';

enum ThreeDotDhikrItem { Delete }

class DhikrItemMenu extends StatelessWidget {
  const DhikrItemMenu({Key key, @required this.viewModel}) : super(key: key);
  final DhikrCounterViewModel viewModel;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<ThreeDotDhikrItem>(
      onSelected: (value) => _onSelected(context, value),
      icon: Icon(
        Icons.more_vert,
        color: Colors.white,
      ),
      itemBuilder: (context) => [
        PopupMenuItem(
          child: Text("Zikiri Sil"),
          value: ThreeDotDhikrItem.Delete,
        ),
        // PopupMenuDivider(
        //   height: 10,
        // ),
      ],
    );
  }

  void _onSelected(BuildContext context, ThreeDotDhikrItem value) async {
    switch (value) {
      case ThreeDotDhikrItem.Delete:
        await ContextUtils.showActionSheet(
          context,
          title: 'İşleme Devam Edilsin mi?',
          message: "${viewModel.dhikrItem.title} Zikri Silinecek",
          actions: [
            SheetAction(
              title: 'Sil',
              color: Colors.red,
              onPress: () {
                viewModel.onDelete();
                navKey.currentState.pop();
              },
            ),
          ],
        );
        break;
      default:
        break;
    }
  }
}
