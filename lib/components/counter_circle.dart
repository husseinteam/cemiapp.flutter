import 'package:cemiapp/entities/index.dart';
import 'package:flutter/material.dart';

class CounterCircle extends StatelessWidget {
  final DhikrItem dhikrItem;

  CounterCircle(this.dhikrItem);

  @override
  Widget build(BuildContext context) {
    final double circleRadius = MediaQuery.of(context).size.shortestSide * 0.34;
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            dhikrItem.title,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w300,
              color: Colors.orange[300],
            ),
          ),
          Text(
            dhikrItem.transcript,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w300,
              color: Colors.green[400],
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox.fromSize(
            size: Size.fromHeight(15.0),
          ),
          Padding(
            padding: EdgeInsets.only(left: circleRadius * 1.7),
            child: Text(
              "${dhikrItem.target}",
              style: TextStyle(
                color: Colors.orange[500],
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          SizedBox.fromSize(
            size: Size.fromRadius(circleRadius),
            child: Stack(
              alignment: Alignment.center,
              children: [
                Positioned.fill(
                  child: CircularProgressIndicator(
                    strokeWidth: 5.0,
                    valueColor: AlwaysStoppedAnimation(
                      Colors.grey[700],
                    ),
                    value: 1.0,
                  ),
                ),
                Positioned.fill(
                  child: CircularProgressIndicator(
                    strokeWidth: 5.0,
                    valueColor: AlwaysStoppedAnimation(
                      Colors.orange[400],
                    ),
                    value: dhikrItem.fixCount() / dhikrItem.target,
                  ),
                ),
                Text(
                  "${dhikrItem.count}",
                  style: TextStyle(
                    color: Colors.blue[500],
                    fontSize: 100,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
