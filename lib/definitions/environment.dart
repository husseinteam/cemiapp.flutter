import 'package:flutter/foundation.dart';

enum Environment {
  localhost,
  development,
  production,
}

const Environment defaultEnvironment =
    kReleaseMode ? Environment.production : Environment.development;
