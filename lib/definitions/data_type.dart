enum DataType {
  text,
  int,
  real,
}

class DataTypeRepr {
  static of(DataType type) {
    switch (type) {
      case DataType.text:
        return "TEXT";
      case DataType.int:
        return "INTEGER";
      case DataType.real:
        return "REAL";
      default:
    }
  }
}
