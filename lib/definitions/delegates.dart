import 'package:cemiapp/entities/index.dart';

typedef AttributerDelegate = void Function(dynamic);
typedef ContextViewGeneratorDelegate = GenericEntity Function();
typedef ActionCallbackDelegate = Function(dynamic);