import 'dart:convert';
import 'package:cemiapp/definitions/index.dart';
import 'package:flutter/material.dart';

abstract class GenericServer {
  @protected
  AttributerDelegate get attributer;
  Map<String, dynamic> toMap();
  String encodeMap() {
    final map = toMap();
    return jsonEncode(map);
  }

  ofSource(String source) {
    attributer(jsonDecode(source));
  }

  ofObject(dynamic object) {
    attributer(object);
  }
}
