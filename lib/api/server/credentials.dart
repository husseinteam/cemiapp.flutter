import 'package:cemiapp/api/server/index.dart';

class Credentials extends GenericServer {
  String identity;
  String password;

  Credentials({
    this.identity,
    this.password,
  });

  @override
  get attributer => (object) {
        this.identity = object["UserName"];
        this.password = object["Password"];
      };

  @override
  Map<String, dynamic> toMap() => {
        "identity": this.identity,
        "password": this.password,
      };
}
