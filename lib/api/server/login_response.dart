import 'package:cemiapp/api/server/index.dart';
import 'package:cemiapp/api/server/token_response.dart';
import 'package:cemiapp/entities/index.dart';

class LoginResponse extends TokenResponse {
  User user;

  LoginResponse({
    this.user,
    String token,
    String message,
    bool success,
    Exception exception,
  }) : super(
          token: token,
          message: message,
          success: success,
          exception: exception,
        );

  @override
  Map<String, dynamic> toMap() => {
        "user": this.user.toMap(),
        "token": this.token,
        "message": this.message,
        "success": this.success,
      };

  @override
  get attributer => (object) {
        this.user = User().fromMap(object["user"], omitId: true);
        this.token = object["token"];
        this.message = object["message"];
        this.success = object["success"];
      };
}
