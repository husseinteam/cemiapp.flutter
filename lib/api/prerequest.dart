import 'package:cemiapp/api/index.dart';
import 'package:cemiapp/definitions/index.dart';
import 'package:cemiapp/api/server/index.dart';

class PreRequest {
  static HttpRequest of(Environment env) {
    final demoCredentials =
        Credentials(identity: "25871077074", password: "8081");
    switch (env) {
      case Environment.localhost:
        return HttpRequest("127.0.0.1:8000", demoCredentials);
      case Environment.development:
        return HttpRequest("127.0.0.1:8000", demoCredentials);
      case Environment.production:
        return HttpRequest(
          "cemiapp.herokuapp.com",
          demoCredentials,
          secured: true,
        );
      default:
        return null;
    }
  }
}
