import 'dart:convert';

import 'package:cemiapp/api/index.dart';
import 'package:cemiapp/definitions/index.dart';
import 'package:cemiapp/api/server/index.dart';
import 'package:http/http.dart' as http;

class HttpRequest {
  HttpRequest(
    this._baseUrl,
    this.credentials, {
    bool secured: false,
  }) {
    _host = secured ? 'https' : 'http';
  }
  final Credentials credentials;
  final String _baseUrl;
  TokenResponse _tokenResponse;
  String _host;

  Future<LoginResponse> login() async {
    return LoginResponse()
      ..ofObject(await _call(ApiMethod.httpGet, "user/signedin"));
  }

  Future _tokenize() async {
    final url = "$_host://$_baseUrl/auth/tokenize";
    final client = http.Client();
    try {
      final response = await client.post(
        url,
        headers: {
          'Content-Type': 'application/json',
        },
        body: credentials.encodeMap(),
      );
      if (response.statusCode == 200) {
        _tokenResponse = TokenResponse()..ofSource(response.body);
      } else {
        try {
          _tokenResponse = TokenResponse()
            ..message = jsonDecode(response.body)["message"].toString();
        } catch (e) {
          _tokenResponse = TokenResponse()..exception = e;
        }
        _tokenResponse.retriedCount++;
      }
    } catch (e) {
      _tokenResponse = TokenResponse()..exception = e;
      _tokenResponse.retriedCount++;
    }
  }

  Future<dynamic> _call(
    ApiMethod method,
    String path, {
    String body,
  }) async {
    if (_tokenResponse == null) {
      await _tokenize();
    } else if (_tokenResponse.success == false) {
      if (_tokenResponse.retriedCount < 3) {
        await _tokenize();
      } else {
        throw _tokenResponse.exception;
      }
    }
    http.Response response;
    final url = "$_host://$_baseUrl/$path";
    final client = AuthClient(_tokenResponse);
    try {
      switch (method) {
        case ApiMethod.httpGet:
          response = await client.get(url);
          break;
        case ApiMethod.httpPost:
          response = await client.post(url, body: body);
          break;
        case ApiMethod.httpPut:
          response = await client.put(url, body: body);
          break;
        default:
          break;
      }
      return jsonDecode(response.body);
    } finally {
      client.close();
    }
  }

  Future<Map<String, dynamic>> single(String path) async {
    return await this._call(ApiMethod.httpGet, path) as Map<String, dynamic>;
  }

  Future<Map<String, dynamic>> post(String path, String body) async {
    return await this._call(ApiMethod.httpPost, path, body: body)
        as Map<String, dynamic>;
  }

  Future<Map<String, dynamic>> put(String path, String body) async {
    return await this._call(ApiMethod.httpPut, path, body: body)
        as Map<String, dynamic>;
  }

  Future<List<TItem>> getList<TItem>(String path) async {
    final iterable =
        (await this._call(ApiMethod.httpGet, path)) as List<dynamic>;
    return iterable.map((item) => item as TItem).toList();
  }
}
